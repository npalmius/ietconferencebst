#IET Conference BibTeX Style File

This repository contains a BibTeX style file for IET Conference papers based on the required reference formats [within this zip file](http://conferences.theiet.org/aht/-documents/latex-template.cfm?type=zip).

The style file is based on one provided by [welf](https://www.blogger.com/profile/14426798962189071382) on his [blog](http://phdtools.blogspot.co.uk/2012/02/iet-conference-paper-reference-style.html) but has been tweaked slightly to fit my needs.

##Samples

###Article

A. N. Author. "Article title", *The Journal*, **1(2)**, pp. 1&ndash;10, (jan 2014).

###Book

A. N. Author. *Book title*, (Publisher, 2014).

####Chapter in a book

A. N. Author, *Book title*, chapter 1, (Publisher, 2014).

####Pages in a book

A. N. Author, *Book title*, pp. 20&ndash;30, (Publisher, 2014).

####Named chapter in a book

A. N. Author. "Chapter title", A. N. Editor
(Ed.), *Book title*, volume 1 of *Book series*, 1st edition, chapter 10, pp. 250&ndash;270, (Publisher, 2014).

###Conference paper

A. N. Author. "Conference paper title", A. N. Editor (Ed.), *Conference proceedings title*, volume 1 of *Conference Series*, pp.
20&ndash;30, Organization, (Publisher, 2014).

###Manual

A. N. Author. *Manual Title*, Organization, Address, 2nd
edition, (2014).

###Master's or Ph.D. thesis

A. N. Author. "Masters or Ph.D. thesis title", Master's / Ph.D. thesis,
School, Address, (2014).

###Conference proceedings

A. N. Editor (Ed.), *Conference proceedings title*, volume 1 of *Conference Series*, Organization, (Publisher, 2014).

###Report

A. N. Author. "Report title", Institution, Address, (2014), [Online].

####Technical Report

A. N. Author. "Technical report title", Technical
Report 15, Institution, Address, (2014).

###URLs & Notes

URLs and notes can be appended to the end of all entry types as follows:

A. N. Author. "Article title", *The Journal*, **1(2)**, pp. 1&ndash;10, (jan 2014), [Online]. Available: http://www.example.com/, notes.

###Reduced author lists

Long author lists can be condensed by removing the last authors and replacing them with *others* in the BibTeX file to give the following output:

A. N. Author, et al. "Article title", *The Journal*, **1(2)**, pp. 1&ndash;10, (jan 2014).

##How to Use

Download the [ietConference.bst](https://bitbucket.org/npalmius/ietconferencebst/raw/master/ietConference.bst) file (you may need to right click and choose 'Save link as...') into the same folder as your paper .tex file and change your bibliography style to:

    \bibliographystyle{ietConference}


##Documentation

Basic documentation with a full set of samples can be found [here](https://bitbucket.org/npalmius/ietconferencebst/src/master/Demo/demo.pdf).

##Support

This style file is released with no warranty at all and is completely unsupported. Any issues can be raised in the [issue tracker](https://bitbucket.org/npalmius/ietconferencebst/issues) and I will try my best to resolve them but make no promises.

If you find and fix an issue please feel free to send me a pull request.

##Disclaimer

This style file has no official backing from the IET but it seemed to work well for the papers that I had to submit.